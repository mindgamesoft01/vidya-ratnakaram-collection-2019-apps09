package com.mindgame.vidyaratnakaram.collection2019

interface AppInterfaces {
    fun loadMenuItemDetailScreen()
    fun loadStartScreen()
    fun loadTopicsScreen()
    fun loadMenuItemlistScreen()
    fun loadBookmarkItemlistScreen()
    fun loadBookMarkItemDetailScreen()
    fun loadPrivacyPolicy()


}